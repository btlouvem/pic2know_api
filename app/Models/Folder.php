<?php

namespace Pic2Know\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Pic2Know\Traits\Uuids;

class Folder extends Model
{
    use Notifiable;
    use SoftDeletes;
    use Uuids;

    public $incrementing = false;

    protected $fillable = [
        'name', 'owner', 'parent',
    ];

    protected $guarded = ['id'];


    public function owner()
    {
        return $this->belongsTo('Pic2Know\Models\User','owner');
    }

    public function parent()
    {
        return $this->belongsTo('Pic2Know\Models\Folder', 'parent');
    }

    public function children()
    {
        return $this->hasMany('Pic2Know\Models\Folder', 'parent', 'id');
    }
}
