<?php

namespace Pic2Know\Http\Controllers\Api;
use Illuminate\Http\Request;
use Pic2Know\Http\Controllers\Controller;
use Pic2Know\Models\User;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\Auth;


class AuthController extends Controller
{
    public function login(Request $request)
    {
        $credentials = $request->only('email','password');

        try{
            $token = JWTAuth::attempt($credentials);

            if($token != null){
                $user = Auth::user();

                $payload = [
                    'name' =>  $user->name,
                    'email' =>  $user->email,
                    'phone' =>  $user->phone,
                    'key' =>  $user->key,
                    'token' => $token
                ];
            }else{
                $payload = ['error'=>'could not create token'];
            }





        }catch (JWTException $ex){
            return response()->json(['error'=>'could not create token'],500);
        }
        if(!$token){
            return response()->json(['error'=>'invalid credentials'],401);
        }

        return response()->json($payload);

    }

    public function logout()
    {
        try{
            JWTAuth::invalidate();
        }catch (JWTException $ex){
            return response()->json(['error'=>'could not invalidate token'],500);
        }

        return response()->json([],204);
    }

    public function refreshToken(Request $request)
    {
        try{
            $bearerToken = JWTAuth::setRequest($request)->getToken();
            $token = JWTAuth::refresh($bearerToken);
            return response()->json(compact('token'));
        }catch (JWTException $ex){
            return response()->json(['error'=>$ex->getMessage()],500);
        }

    }

    public function register(Request $request)
    {
        $user_fields = [
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'phone' => $request->input('phone'),
            'password' => \Hash::make($request->input('password'))
        ];

        try{
            $user = new User($user_fields);
            $user->save();

            $token = JWTAuth::attempt([
                'email' => $user->email,
                'password' => $request->input('password')
            ]);

            $payload = [
                'name' =>  $user->name,
                'email' =>  $user->email,
                'phone' =>  $user->phone,
                'key' =>  $user->key,
                'token' => $token
            ];
        }catch (\Exception $ex){
            $payload = [
                'error' =>  $ex->getMessage()
            ];
        }catch (JWTException $ex){
            $payload = [
                'error' =>  $ex->getMessage()
            ];
        }

        return response()->json($payload);
    }


}