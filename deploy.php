<?php
namespace Deployer;

require 'recipe/laravel.php';

// Configuration

set('repository', 'https://btlouvem@gitlab.com/btlouvem/pic2know_api.git');
set('git_tty', true); // [Optional] Allocate tty for git on first deployment
add('shared_files', []);
set('branch', "development");
set('writable_use_sudo', true);
// add('shared_dirs', []);
// add('writable_dirs', []);
set('default_stage', 'production');

// Hosts

host('52.37.223.0')
    ->stage('production')
    ->user("ubuntu")
    ->port(22)
    ->set('deploy_path', '/var/www/html/pic2know/api')
    ->identityFile('/root/sylabs.pem')
    ->forwardAgent(true)
    ->multiplexing(true);



// Tasks

desc('Restart PHP-FPM service');
task('php-fpm:restart', function () {
    // The user must have rights for restart service
    // /etc/sudoers: username ALL=NOPASSWD:/bin/systemctl restart php-fpm.service
    run('sudo service php7.0-fpm restart');
});
after('deploy:symlink', 'php-fpm:restart');

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

// Migrate database before symlink new release.

before('deploy:symlink', 'artisan:migrate');
