<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => '/v1', 'middleware' => 'cors'], function () {

    /**
     * Login Route
     */
    Route::post('login','Api\AuthController@login');
    Route::post('refresh_token','Api\AuthController@refreshToken');

    Route::post('register','Api\AuthController@register');

    Route::group(['middleware' => 'jwt.auth'], function () {

        Route::resource('folders', 'Api\FolderController');
        /**
         * Logout Route
         */
        Route::get('logout','Api\AuthController@logout');
        //teste
    });
});

