<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFoldersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('folders', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');

            $table->string('name')->length(255);

            $table->integer('owner')->unsigned();
            $table->uuid('parent')->nullable();

            $table->foreign('owner')->references('id')->on('users');
            $table->foreign('parent')->references('id')->on('folders');

            $table->boolean('is_parent')->default(true);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('folders');
    }
}
