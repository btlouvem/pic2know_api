<?php

use Illuminate\Database\Seeder;
use Pic2Know\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class)->create([
            'name'=>'Bruno Louvem',
            'email'=>'btlouvem@gmail.com',
            'password'=>bcrypt(123456)
        ]);
    }
}
